/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.archana;

import com.google.gson.Gson;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Archana
 */
public class FriendsInvitation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        double delhiLatitude = 28.521134;
        double delhiLongitude = 77.206567;

        String jsonString = new String(Files.readAllBytes(Paths.get("./friends.json")));
        Gson gson = new Gson();
        Friend[] friends = gson.fromJson(jsonString, Friend[].class);
        ArrayList<Friend> friendArray = new ArrayList<>();
        for (Friend friend : friends) {
            double distance = computeDistanceFromDelhi(delhiLatitude, delhiLongitude, friend.latitudes, friend.longitudes);
            if (distance <= 100) {
                insertByOrder(friendArray, friend);
            }
        }
        for (Friend friend : friendArray) {
            System.out.println("ID:"+friend.id+" , Name:"+friend.name);
        }
    }

    private static void insertByOrder(ArrayList<Friend> friendArray, Friend friend) {
        int i = 0;
        for (i = friendArray.size()-1; i >= 0; i--) {
            if (friendArray.get(i).id < friend.id) {
                break;
            }
        }
        i=i<0?0:i;
        friendArray.add(i, friend);
    }

    private static double computeDistanceFromDelhi(double delhiLatitude, double delhiLongitude, Double latitudes, Double longitudes) {
        double earthRadius = 6371;//km
        double distance = earthRadius * Math.acos(Math.sin(delhiLatitude) * Math.sin(latitudes) + Math.cos(delhiLatitude) * Math.cos(latitudes) * Math.cos(delhiLongitude - longitudes));
        return distance;
    }

}
