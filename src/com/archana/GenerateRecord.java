/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.archana;

import com.google.gson.Gson;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

/**
 *
 * @author Archana
 */
public class GenerateRecord {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Friend[] friend = new Friend[50];
        for (int i = 0; i < 50; i++) {
            friend[i] = new Friend();
            friend[i].id = i;
            friend[i].name = Character.toString((char) (new Random().nextInt(90 - 65 + 1) + 65));
            friend[i].latitudes = (new Random().nextDouble()*(90 - (-90) + 1)) + (-90);
            friend[i].longitudes = (new Random().nextDouble()*(180 - (-180) + 1)) + (-180);
        }
        Gson gson = new Gson();
        String jsonString = gson.toJson(friend);
        Files.write(Paths.get("./friends.json"), jsonString.getBytes());
    }

}
